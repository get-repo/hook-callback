<p align="center">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/17739319/hook_logo.png" height=100 />
</p>

<h1 align=center>Hook Callback</h1>
<h4 align=center style="border-bottom: 3px solid #999; padding-bottom: 15px;">
    Just a PHP trait to hook into
</h4>

<br/>
This trait allows you to create hooks and callbacks in any PHP class.
If these hook methods exists, they will be executed.


<br/><br/>
## Installation

This is installable via [Composer](https://getcomposer.org/):

    composer config repositories.get-repo/hook-callback git https://gitlab.com/get-repo/hook-callback.git
    composer require get-repo/hook-callback


<br/><br/>
## How to use the *createHookCallback($options)* methods

The method `createHookCallback(array $options)` allows you to create callbacks with options:
```php
[
    // will look for a method called test%PATTERN%(), call if exists and is callbale.
    'name' => 'test',
    // method parameters. Will call is with parameters test%PATTERN%(1, 2).
    'parameters' => [1, 2],
    // Method patterns to look for. testHook() and testAction().
    'patterns' => ['%sHook', '%sAction'],
    // Stop if one method pattern is found.
    'return_first' => true,
]
```

<br/><br/>
## Example

Let's say we have a simple Controller PHP class, using the `HookCallbackTrait`.
```php
<?php

namespace App\Controller;

use GetRepo\HookCallback\HookCallbackTrait;

class MyController
{
    use HookCallbackTrait;

    public function indexAction($request, $id)
    {
        $this->createHookCallback([
            'name' => 'beforeIndex',
            'parameters' => [$id],
            'patterns' => ['%Action'],
        ]);
    }

    private function beforeIndexAction($id)
    {
        // do something
    }
}
```
The method `beforeIndexAction()` will be called.


<br/><br/><br/><br/>
