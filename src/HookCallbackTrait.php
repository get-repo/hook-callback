<?php

namespace GetRepo\HookCallback;

use Symfony\Component\OptionsResolver\OptionsResolver;

trait HookCallbackTrait
{
    /** @var string[] */
    protected array $hookCallbackPatterns = ['%sHook'];
    protected ?object $hookCallbackTarget = null;

    /**
     * @param array{
     *     'name': string,
     *     'target'?: object,
     *     'parameters'?: array<mixed>,
     *     'return_first'?: bool,
     *     'patterns'?: array<string>,
     * } $options
     */
    protected function createHookCallback(array $options): mixed
    {
        // resolve options
        $resolver = new OptionsResolver();
        // name of the hook
        $resolver->setRequired('name');
        $resolver->setAllowedTypes('name', 'scalar');
        // the object target to call against
        $resolver->setDefault('target', $this->hookCallbackTarget ?? $this);
        $resolver->setAllowedTypes('target', 'object');
        // callback parameters
        $resolver->setDefault('parameters', []);
        $resolver->setAllowedTypes('parameters', 'array');
        // return first callback found
        $resolver->setDefault('return_first', true);
        $resolver->setAllowedTypes('return_first', 'boolean');
        // return first callback found
        $resolver->setDefault('patterns', ['%sHook']);
        $resolver->setAllowedTypes('patterns', 'array');
        // resolve options
        $options = $resolver->resolve($options);

        $results = [];
        foreach ($options['patterns'] as $pattern) {
            $method = sprintf($pattern, $options['name']);

            $callback = [$options['target'], $method];
            if (is_callable($callback)) {
                $r = call_user_func_array($callback, $options['parameters']);
                if ($options['return_first']) {
                    return $r;
                }

                $results[$method] = $r;
            }
        }

        return $results;
    }
}
