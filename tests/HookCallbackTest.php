<?php

namespace Test;

use GetRepo\HookCallback\HookCallbackTrait;
use PHPUnit\Framework\TestCase;

class HookCallbackTest extends TestCase
{
    use HookCallbackTrait;

    private ?string $hookCalled = null;

    public function testMethodNotCalled(): void
    {
        $value = $this->createHookCallback([
            'name' => 'method_does_not_exists',
        ]);
        $this->assertIsArray($value);
        $this->assertEmpty($value);
        $this->assertNull($this->hookCalled);
    }

    public function testMethodCalled(): void
    {
        $value = $this->createHookCallback([
            'name' => 'a',
        ]);
        $this->assertNull($value);
        $this->assertEquals('Test\HookCallbackTest::aHook', $this->hookCalled);
    }

    public function testMethodParameters(): void
    {
        $value = $this->createHookCallback([
            'name' => 'a',
            'parameters' => [1],
        ]);
        $this->assertNull($value);
        $this->assertEquals('Test\HookCallbackTest::aHookp1_sent', $this->hookCalled);
    }

    public function testMethodParameterReference(): void
    {
        $ref = 'ref';
        $value = $this->createHookCallback([
            'name' => 'Ref',
            'parameters' => [&$ref],
        ]);
        $this->assertNull($value);
        $this->assertEquals('Test\HookCallbackTest::refHookp1_ref', $this->hookCalled);
        $this->assertEquals('p1_ref', $ref);
    }

    public function testMethodPatternDoesNotExists(): void
    {
        $value = $this->createHookCallback([
            'name' => 'a',
            'patterns' => ['%sPatternDoesNotExists'],
        ]);
        $this->assertIsArray($value);
        $this->assertEmpty($value);
        $this->assertNull($this->hookCalled);
    }

    public function testMethodPattern(): void
    {
        $value = $this->createHookCallback([
            'name' => 'a',
            'patterns' => ['%sCustom'],
        ]);
        $this->assertEquals($value, 'custom');
        $this->assertEquals('Test\HookCallbackTest::aCustom', $this->hookCalled);
    }

    public function testMethodPatternsList(): void
    {
        $value = $this->createHookCallback([
            'name' => 'a',
            'patterns' => ['%sShouldFail', '%sHook'],
        ]);
        $this->assertNull($value);
        $this->assertEquals('Test\HookCallbackTest::aHook', $this->hookCalled);
    }

    public function testReturnFirstEnabled(): void
    {
        $value = $this->createHookCallback([
            'name' => 'a',
            'patterns' => ['%sShouldFail', '%sHook', '%sCustom'],
            // also test return_first = true by default
        ]);
        $this->assertNull($value);
        $this->assertEquals('Test\HookCallbackTest::aHook', $this->hookCalled);

        $value = $this->createHookCallback([
            'name' => 'a',
            'patterns' => ['%sShouldFail', '%sCustom', '%sHook'],
            'return_first' => true,
        ]);
        $this->assertEquals('custom', $value);
        $this->assertEquals('Test\HookCallbackTest::aCustom', $this->hookCalled);
    }

    public function testReturnFirstDisabled(): void
    {
        $value = $this->createHookCallback([
            'name' => 'a',
            'patterns' => ['%sShouldFail', '%sHook', '%sCustom'],
            'return_first' => false,
        ]);
        $this->assertIsArray($value);
        $this->assertEquals(['aHook' => null, 'aCustom' => 'custom'], $value);

        // test different order
        $value = $this->createHookCallback([
            'name' => 'a',
            'patterns' => ['%sCustom', '%sShouldFail', '%sHook'],
            'return_first' => false,
        ]);
        $this->assertIsArray($value);
        $this->assertEquals(['aCustom' => 'custom', 'aHook' => null], $value);
    }

    public function testTarget(): void
    {
        $target = new class () {
            public bool $called = false;
            public function testHook(): string
            {
                $this->called = true;
                return 'called';
            }
        };
        $this->assertFalse($target->called);
        $value = $this->createHookCallback([
            'name' => 'whatever', // not called
            'target' => $target,
        ]);
        $this->assertFalse($target->called);
        $this->assertIsArray($value);
        $this->assertEmpty($value);

        $value = $this->createHookCallback([
            'name' => 'test', // called
            'target' => $target,
        ]);
        $this->assertTrue($target->called);
        $this->assertIsString($value);
        $this->assertEquals('called', $value);
    }

    private function aHook(bool $param1 = false): void // @phpstan-ignore-line
    {
        if ($param1) {
            $param1 = 'p1_sent';
        }
        $this->hookCalled = __METHOD__ . $param1;
    }

    private function refHook(string &$param1 = null): void // @phpstan-ignore-line
    {
        if ($param1) {
            $param1 = 'p1_ref';
        }
        $this->hookCalled = __METHOD__ . $param1;
    }

    private function aCustom(): string // @phpstan-ignore-line
    {
        $this->hookCalled = __METHOD__;

        return 'custom';
    }
}
